/*
CPSO1011 XYZ 
Monstersope Arduino Conolos 
Version : 05
Author: BB. 

*/

#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include "Adafruit_TLC5947.h"
#include <ezButton.h>
#include <ArduinoJson.h>

/// *********** Buttons

const int BUTTON_NUM = 7;
const int BUTTON_PINS[] = {41, 42, 43, 44, 47, 45, 46};
int lastActiveButton = 0;


const int CRANK_PIN = 40;
int CRANK_COUNT = 0;
const int THREE_WAY_PIN[] = {15, 14, 16};
int THREE_WAY_POS = 0;
const int SEVEN_WAY_PINS[] = {35, 34, 33, 32, 37, 36, 31};   // {31, 32, 33, 34, 35, 36, 37};
const int LX_RELAY_PINS[] = {17, 18, 19, 20, 21};
const byte LX_PIN_NUM = 5;

const int PUSH_TO_TALK_PIN = 49;

ezButton buttonArray[] = {
  ezButton(BUTTON_PINS[0]),
  ezButton(BUTTON_PINS[1]),
  ezButton(BUTTON_PINS[2]),
  ezButton(BUTTON_PINS[3]),
  ezButton(BUTTON_PINS[4]),
  ezButton(BUTTON_PINS[5]),
  ezButton(BUTTON_PINS[6]),
  ezButton(BUTTON_PINS[7])
};

ezButton sevenWayArray[] = {
  ezButton(SEVEN_WAY_PINS[0]),
  ezButton(SEVEN_WAY_PINS[1]),
  ezButton(SEVEN_WAY_PINS[2]),
  ezButton(SEVEN_WAY_PINS[3]),
  ezButton(SEVEN_WAY_PINS[4]),
  ezButton(SEVEN_WAY_PINS[5]),
  ezButton(SEVEN_WAY_PINS[6]),
  ezButton(SEVEN_WAY_PINS[7])
};

ezButton three_way_switchArray[] = {
  ezButton(THREE_WAY_PIN[0]),
  ezButton(THREE_WAY_PIN[1]),
  ezButton(THREE_WAY_PIN[2]),
};


// init crank
ezButton crank(CRANK_PIN);
// int push to tlak

//ezButton ptt(PUSH_TO_TALK_PIN);


//String buttonColorName[] = {"Orange", "Red", "Yellow", "White", "Pink", "Violet", "Green"};

String buttonColorName[] = {"1", "7", "3", "4", "6", "5", "2"};
int buttonColorCode[7][3] = {
  {256, 50, 0},    // Ornage
  {255, 0, 0},      //red
  {255, 128 , 0 }, // yellow
  {136, 136, 136},  // white
  {241, 20, 254}, // pink
  {50, 0, 200}, // violet
  {0, 255, 0}   // green
};


// PWM driver
// How many boards do you have chained?
#define NUM_TLC5974 1

#define data   26//23//4 white 
#define clock   27// 24//5 red
#define latch   28//25//6  black
#define oe  -1  // set to -1 to not use the enable pin (its optional)

Adafruit_TLC5947 tlc = Adafruit_TLC5947(NUM_TLC5974, clock, data, latch);

// Enter a MAC address for your controller below.
byte mac[] = {
  0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02
};

byte ip[] = { 192, 168, 206, 30 };
byte server[] = { 10, 100, 95, 135 };  //  Server 192.168.206.229 10.100.95.88
//byte server[] = { 159, 203, 47, 62}; //  could test
int tcp_port = 3100;

EthernetClient client;

// An EthernetUDP instance to let us send and receive packets over UDP


void setup() {
  Serial.print("hello");

  for (byte i = 0; i < BUTTON_NUM; i++) {
    buttonArray[i].setDebounceTime(50); // set debounce time to 50 milliseconds
  }

  for (byte i = 0; i < 3; i++) {
    three_way_switchArray[i].setDebounceTime(50); // set debounce time to 50 milliseconds
  }

  for (byte i = 0; i < 7; i++) {
    sevenWayArray[i].setDebounceTime(2000); // set debounce time to 50 milliseconds
    // SET THE VALUE TO AVOID CROSS TRIGGEIRNG DURING MOVMNET 
  }
  for (byte i = 0; i < 5; i++) {
    pinMode(LX_RELAY_PINS[i], OUTPUT);
  }

  crank.setDebounceTime(50);

 //ptt.setDebounceTime(50); // REPLACED WITH usb SPACE 

  // PWM init:
  Serial.println("TLC5974 test");
  tlc.begin();
  if (oe >= 0) {
    pinMode(oe, OUTPUT);
    digitalWrite(oe, LOW);
  }

  setAllButtonOff();


  //!!!!!! Etherent Init: !!!!!!!!\\\\\\

  // You can use Ethernet.init(pin) to configure the CS pin
  Ethernet.init(10);  // Most Arduino shields

  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // start the Ethernet connection:
  Serial.println("Initialize Ethernet with DHCP:");
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
    } else if (Ethernet.linkStatus() == LinkOFF) {
      Serial.println("Ethernet cable is not connected.");
    }
    
    //while (true) {
    //  delay(1);
    //}

  }
  // print your local IP address:
  Serial.print("My IP address: ");
  Serial.println(Ethernet.localIP());

  connectToBrain();
}

void connectToBrain() {
  if (client.connect(server, tcp_port)) { // Connection to server.js
    Serial.println("Connected to barin");
    client.println();
  } else {
    Serial.println("connection failed");
  }
  //setAllButtonOn();

}

const uint32_t timeoutMs = 4000 ;
const uint32_t crankTimeoutMs = 10000 ;

uint32_t startAtMs = millis();
bool cran_active = false;


void loop() {

  /*
    if ( millis() - startAtMs > timeoutMs ) {
      Serial.println("time");
      startAtMs = millis();
    }
  */
  buttonHandler();
  threeWaySwitchHandler();
  sevenWaySwitchHandler();
  crankHandler();
//   pttHandler();

}

//Send to server
// Send data to the brain
/* V02
// Send data to the BRAIN
void apiSend(String action, String value) {
  StaticJsonDocument<200> doc;
  doc["action"] = action;
  doc["value"] = value;

  serializeJson(doc, Serial);
  Serial.println();
  serializeJsonPretty(doc, client);
}
*/

// V01 
void apiSend(String action, String value)
{
  String cmd = ("{\""  + action +  "\":\"" +  value  + "\"}");
  const char *cmd2 = cmd.c_str();
  if (!client.connected()) {
    connectToBrain();
    Serial.print("Try to reconnect to Brain : ");
  }

  client.write(cmd2);

  Serial.print("Action: ");
  Serial.println(action);
  Serial.print("Value: ");
  Serial.println(value);
}


// crank hadnler need to turn 3 times and send signal
void crankHandler() {

  crank.loop();

  if (crank.isPressed()) {
    Serial.print("The Crank signal ");
    CRANK_COUNT++;
    Serial.println(CRANK_COUNT);
    cran_active = true;
  }

  // if crank is> 0 than avaibel timeout tha turn it off
  if (cran_active) {
    if ( millis() - startAtMs > crankTimeoutMs ) {
      startAtMs = millis();
      Serial.println("time_out crank");
      cran_active = false;
      CRANK_COUNT = 0;
    }
  }
  else {
    startAtMs = millis();
  }

  if (CRANK_COUNT >= 3) {
    CRANK_COUNT = 0;
    apiSend("Start", "true");
  }
}


// Deal with Buttons:
void buttonHandler() {

  for (byte i = 0; i < BUTTON_NUM; i++)
    buttonArray[i].loop(); // MUST call the loop() function first

  for (byte i = 0; i < BUTTON_NUM; i++) {
    if (buttonArray[i].isPressed()) {
      Serial.print("The button ");
      Serial.print(i + 1);
      Serial.println(" is pressed");

      // Set Color:
      setButtonColor(i);
      lastActiveButton = i + 1;
      lx_trigger(THREE_WAY_POS, i + 1);

      // Send Command:
      String active_color = buttonColorName[i];
      Serial.print("Color Activated:");
      Serial.println(active_color);


      apiSend("Color", active_color);
    }
  }
}


void setButtonColor(int bt) {

  // set RGB color of active button
  //tlc.setLED(bt,  buttonColorCode[bt][0] * 255 , buttonColorCode[bt][1] * 255 , buttonColorCode[bt][2] * 255);
  tlc.setLED(bt,  buttonColorCode[bt][0] * 15 , buttonColorCode[bt][1] * 15 , buttonColorCode[bt][2] * 15);

  // set black for the rest of the buttons
  for (int b = 0 ; b < BUTTON_NUM ; b++) {
    if (b !=  bt) {
      tlc.setLED(b,  0 , 0 , 0);
    }
  }
  tlc.write();
}

void setAllButtonOff() {
  for (int b = 0 ; b < BUTTON_NUM ; b++) {
    tlc.setLED(b,  0 , 0 , 0);
  }
  tlc.write();
}


void setAllButtonOn() {
  for (int b = 0 ; b < BUTTON_NUM ; b++) {
    tlc.setLED(b,  255 , 255 , 255);
  }
  tlc.write();
}

// Deal with Buttons:
void threeWaySwitchHandler() {

  for (byte i = 0; i < 3; i++)
    three_way_switchArray[i].loop(); // MUST call the loop() function first

  for (byte i = 0; i < 3; i++) {
    if (three_way_switchArray[i].isPressed()) {
      Serial.print("The Switch ");
      Serial.print(i + 1);
      Serial.println(" is clicked");
      THREE_WAY_POS = i;
      lx_trigger(THREE_WAY_POS, lastActiveButton);
      apiSend("Intensity", String(i + 1));
    }
  }
}



// Deal with Buttons:
void sevenWaySwitchHandler() {

  for (byte i = 0; i < 7; i++)
    sevenWayArray[i].loop(); // MUST call the loop() function first

  for (byte i = 0; i < 7; i++) {
    if (sevenWayArray[i].isPressed()) {
      Serial.print("The 7 Switch ");
      Serial.print(i + 1);
      Serial.println(" is clicked");


      //{"Ambience": "5"}
      apiSend("Ambience", String(i + 1));
    }
  }
}

const byte numPins = 5;

void lx_trigger(int intensity , int color) {

  byte num = 7 * intensity + color ;

  //byte num = Serial.read(); // Get num from somewhere
  for (byte i = 0; i < LX_PIN_NUM; i++) {
    byte state = bitRead(num, i);
    digitalWrite(LX_RELAY_PINS[i], !state);
    Serial.print(state);
  }

  Serial.println();

}

/* DISABLED
void pttHandler() {

  ptt.loop();

  if (ptt.isPressed()) {
    Serial.print("The Push to talk ACTIVE ");
     apiSend("Mute", "0");
  }

  if (ptt.isReleased()) {
    Serial.print("The Push to talk OFF ");
     apiSend("Mute", "1");
  }
}
*/ 
